## **Question 1:**

How would you speed up a single page application?

Introducing Server Side rendering, which is native to some web frameworks like Nextjs or Vuejs can help reduce client processing time.
Implementing code splitting to your SPA so the browser doesnt need to download all the code for your other pages and components.

## **Question 2:**

Explain the `this` keyword and it's quirks in JavaScript

The `this` keyword references the object that is currently calling the function.
Some quirks include its use in ES6 arrow functions. In arrow functions, the scope of `this` is set lexically, which means it inherits `this` from the outer function where the arrow function is defined.
Another quirk is that the `this` keyword can also have a global context, so it has access to the window object of your web browser.

## **Question 3:**

What is the difference between border-box and content-box?
content-box is calculated by adding the dimensions of your element's padding, border and content.
border-box is calculated by automatically adjusting the dimensions of padding, border and content to fit the given height and width.

## **Question 4:**

What's the difference between == and === operators?
=== means you are comparing values with strict equality. This strict equality checks that both the type and value have to be the same.
== is more leanient and is referred to loose equality. Javascript will try to convert the values into a similar type and compare the values.

## **Question 5:**

What is accessibility? How do you achieve it?
Accessibility is designing an app that makes it usable for people with various disabilities such as vision problems, hearing impairment, etc.
There are many ways to achieve digital accessibility. Many common practices for web developers to achieve this include adding alt text to images, making your site keyboard friendly, structuring your headers correctly, semantic html tags, using accessible color schemes, using default html tags.

## **Question 6:**

What is the difference between session storage, local storage and cookies?
Session storage only stores data for a session. So if you close the browser or a tab the data stored will be gone. Session storage is only read client-side so there is no need to worry about sending it to the server.
Local Storage can store data on the client's computer. It saves key/value pairs into a browser but it can only be accessed client side with Javascript and HTML.
Cookies can more server oriented, as they store data that has to be sent back to the server with requests sent from the client. An expiration time can be set on cookies and can also be made more secure with the httpOnly flag set to true.

## **Question 7:**

How does hoisting work in JavaScript, and what is the order of hoisting?
Hoisting is a mechanism where functions and variables are moved to the top of their scope before code execution. With hoisting, variable assignment takes precedence over function declaration and function declarations take precedence over variable declarations.

## **Question 8:**

What is the difference in usage of callback, promise, and async/await?
A callback is a function that is passed to another function. When the first function call is done it will run this second function. The most popular use of this is with asynchronous actions and chaining on .then() and .catch().
Javascript promises are also used to handle the asynchronous result of an operation. It has two outcomes, it will either be resolved when the operation succeeds or it is rejected. A promise can defer the execution of a block of code until an async request is completed.
Async/await is syntactic sugar for promises and a more modern solution as oposed to callback functions. It makes asynchronous code look more synchronous and is much cleaner. The await keyword here is used in a function you define as async. This makes sure that promises returned in the async function wait for a resolution to execute the next block of code. If there are multiple promises one after another, they will all wait for eachother.
