## **Question 1:**

Why us React over other JS frameworks?
React is less opinionated in code as opposed to angular. React makes it simpler to create rich, interactive UI's. It allows the programmer to build stand alone UI components (a common practice) and even entire user interfaces. React also implements what is known as virtual-dom - a copy of the real dom tree. React compares the virtual dom to the real DOM for the latest updates and provides the required minimum changes to the real DOM, which makes it very fast to develop with. React also has a very rich ecosystem and community surrounding it, improving developer experience.

## **Question 2:**

What happens during the lifecycle of a React component?
A react component has four stages in its lifecycle.
Mounting is the stage where an instance of a component is being created and inserted into the DOM. Updating is whenever there is a change in the component's state or props. Render occurs whenever a component gets updated, it has to re-render the HTML with the new changes to the DOM. Unmounting is the next phase when a component is removed from the DOM.

## **Question 3:**

What are stateless components?
Stateless components can also be referred to as presentational components. They print out or present the data or state that is passed to them via props. Stateless components are often very reusable and traditionally were created as function components.

## **Question 4:**

Differentiate between Real DOM and Virtual DOM.
The Virtual DOM is a copy of the Real DOM. Whenever a change in a React component happens, it re-renders the Virtual DOM first, compares the old version to the new one, and eventually only the required minimum is changed in the Real DOM instead of re-rendering the entire UI.
A traditional DOM is an object oriented representation of a website, where elements of HTML become nodes in the DOM. It is faster to use the Virtual DOM to make updates to your website than just the Real DOM alone.

## **Question 5:**

How would you improve SEO of a react Application?
I would start by implementing Server Side Rendering. With server-side rendering, browsers and Google bots get HTML files with all the content. Google bots can index the page properly and rank it higher. This can be implemented using a framework like Gatsby or Next.

## **Question 6:**

What are the limitations of React?
Since React is a relatively new technology, documentation may be lacking still. Learning curve is steep for new developers. React also focuses heavily on the UI layer of an application, and will require the developer to use other technologies to createa a complete project.

## **Question 7:**

What are some advantages of using React?
Virtual-DOM helps in making the overall development process for user interfaces much faster. React allows the creation of reusable components that can be implemented in other parts of the project. React hooks that were recently introduced also help share state and reuse logic easier among your components.

## **Question 8:**

What is server-side rendering, and what problems does it solve?
Server-side rendering is a way to convert HTML files on the server into a fully rendered HTML page for the client. Your browser makes a request for the web page from the server, which instantly responds by sending a fully rendered page to the client. SSR by nature enables pages to load faster, but it also helps users with slow internet connection load web pages more efficiently. Greatly improves SEO and helps your web site rank higher.

## **Question 9:**

List and briefly describe some security attacks on the frontend?.
Cross Site Scripting Attack is a type of attack where the user inputs a malicious script into an element of the web app or the search bar url. When other users access the web application, since the browser does not know that it is malicious as it was served by the backend, this malicious script is executed.

Cross-Site Request Forgery is an attack that tricks an authenticated user into submitting a malicious request. When this request reaches the backend, the server will have no way to distinguish between a malicious request and a legitimate request.

Denial of Service Attack is an attack where a user seeks to make a server resource unavailable to the actual users by disrupting the services of a server connected to the internet. Usually the perpetrator overloads the system by a huge number of requests in a very short interval of time and prevent legitimate requests to be served
