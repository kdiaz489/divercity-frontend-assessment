import { createTheme } from '@material-ui/core/styles';
import cyan from '@material-ui/core/colors/cyan';

export const theme = createTheme({
  palette: {
    primary: {
      main: cyan[500],
    },
  },
});
