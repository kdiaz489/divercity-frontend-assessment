import {
  Card,
  CardContent,
  CardActions,
  makeStyles,
  Typography,
  Grid,
  Box,
  Button,
} from '@material-ui/core';
import cyan from '@material-ui/core/colors/cyan';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: 40,
    paddingBottom: 40,
    backgroundColor: '#fefefe',
    height: '100vh',
  },
  title: {
    color: cyan[500],
  },
  card: {
    width: 325,
    padding: '12px',
    margin: '40px 0',
  },
}));

const TeamCard = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const { setTeam, setDialogOpen, team } = props;

  const currTeam = useSelector((state) =>
    state.teams.teams.find((t) => t._id === team._id)
  );
  let handleClick = (e) => {
    setTeam(currTeam);
    setDialogOpen(true);
  };

  const handleEditClick = (e) => {
    history.push(`/teams/edit/${team._id}`);
  };

  return (
    <div>
      <Card className={classes.card}>
        <CardContent>
          <Typography
            variant='h5'
            component='h2'
            className={classes.title}
            gutterBottom>
            Team
          </Typography>
          <Grid container spacing={1}>
            <Grid item>
              <Box
                display='flex'
                flexDirection='column'
                justifyContent='center'
                height='100%'>
                <Typography>ID: {team._id}</Typography>
                <Typography>Team Members: {team.people.length}</Typography>
              </Box>
            </Grid>
          </Grid>
        </CardContent>
        <CardActions style={{ justifyContent: 'space-between' }}>
          <Button variant='outlined' size='small' onClick={handleClick}>
            View Details
          </Button>
          <IconButton size='small' onClick={handleEditClick}>
            <EditIcon />
          </IconButton>
        </CardActions>
      </Card>
    </div>
  );
};

export default TeamCard;
