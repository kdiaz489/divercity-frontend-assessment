import NavBar from './NavBar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import useEditDepartment from '../hooks/useEditDepartment';

const useStyles = makeStyles((theme) => ({
  form: {
    marginTop: 120,
    padding: 20,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textfield: {
    margin: 20,
  },
  formControl: {
    width: 500,
    marginBottom: '20px',
  },
}));

const EditDepartment = (props) => {
  const classes = useStyles();
  const {
    department,
    users,
    teams,
    departmentData,
    setDepartmentData,
    handleChange,
    handleSubmit,
  } = useEditDepartment(props);

  if (!department || Object.keys(department).length === 0) {
    return (
      <NavBar>
        <h1>Loading...</h1>
      </NavBar>
    );
  } else {
    return (
      <NavBar>
        <Typography variant='h2' align='center'>
          Edit Department
        </Typography>

        <form className={classes.form} onSubmit={handleSubmit}>
          <FormControl className={classes.formControl}>
            <InputLabel id='inCharge-label'>In Charge</InputLabel>
            <Select
              className={classes.root}
              labelId='inCharge-label'
              id='inCharge'
              fullWidth
              value={departmentData.inCharge}
              onChange={handleChange}
              inputProps={{
                name: 'inCharge',
              }}>
              <MenuItem value=''>
                <em>None</em>
              </MenuItem>
              {users.map((user) => (
                <MenuItem key={user._id} value={user._id}>
                  {user.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <FormControl className={classes.formControl}>
            <InputLabel id='teams-label'>Teams</InputLabel>
            <Select
              labelId='teams-label'
              id='teams'
              multiple
              fullWidth
              value={departmentData.teams}
              onChange={handleChange}
              input={<Input />}
              inputProps={{
                name: 'teams',
              }}>
              <MenuItem value=''>
                <em>None</em>
              </MenuItem>
              {teams.map((team) => (
                <MenuItem key={team._id} value={team._id}>
                  {team._id}
                </MenuItem>
              ))}
            </Select>
          </FormControl>

          <Button type='submit'>Submit</Button>
        </form>
      </NavBar>
    );
  }
};

export default EditDepartment;
