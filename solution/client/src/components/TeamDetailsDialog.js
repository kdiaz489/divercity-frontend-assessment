import Dialog from './Dialog';
import { Typography } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
const TeamDetailsDialog = (props) => {
  const { open, team, dialogTitle, setOpen, handleClose } = props;

  return (
    <>
      {team && (
        <Dialog
          open={open}
          disableSubmit={true}
          dialogTitle={dialogTitle}
          dialogTitle={'Team Details'}
          dialogText={'Details about team members and lead'}
          onClose={handleClose}
          setOpen={setOpen}
          aria-labelledby='form-dialog-title'>
          <Divider />
          <Typography>Team Lead</Typography>
          <Divider />
          <Typography>{team.teamLead.name}</Typography>
          <br />

          <Divider />
          <Typography>Team Members</Typography>
          <Divider />
          <Typography>
            {team.people.map((person) => (
              <>
                {person.name}
                <br />
              </>
            ))}
          </Typography>
        </Dialog>
      )}
    </>
  );
};

export default TeamDetailsDialog;
