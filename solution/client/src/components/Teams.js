import TeamCard from './TeamCard';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/styles';
import NavBar from './NavBar';
import { Button } from '@material-ui/core';
import useTeams from '../hooks/useTeams';
import TeamDetailsDialog from './TeamDetailsDialog';
import AddTeamDialog from './AddTeamDialog';

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: 40,
    paddingBottom: 40,

    height: '100vh',
  },
}));

const Teams = (props) => {
  const {
    open,
    team,
    teams,
    setTeam,
    setOpen,
    handleOpen,
    handleClick,
    openAddTeam,
    setOpenAddTeam,
    handleAddTeamClick,
  } = useTeams(props);

  const classes = useStyles();

  if (teams.length === 0 || teams === undefined) {
    return (
      <NavBar>
        <Typography variant='h2' align='center'>
          Loading...
        </Typography>
      </NavBar>
    );
  }

  return (
    <NavBar>
      {team && <TeamDetailsDialog team={team} open={open} setOpen={setOpen} />}
      <AddTeamDialog open={openAddTeam} setOpen={setOpenAddTeam} />

      <Typography variant='h2' align='center'>
        All Teams
      </Typography>
      <Box
        display='flex'
        flexDirection='row'
        flexWrap='wrap'
        justifyContent='space-around'>
        {teams.map((team) => (
          <TeamCard
            key={team._id}
            setTeam={setTeam}
            setDialogOpen={setOpen}
            team={team}
          />
        ))}
      </Box>
      <Box
        onClick={handleOpen}
        display='flex'
        justifyContent='flex-end'
        alignItems='flex-end'>
        <Button onClick={handleAddTeamClick}>Add Team</Button>
      </Box>
    </NavBar>
  );
};

export default Teams;
