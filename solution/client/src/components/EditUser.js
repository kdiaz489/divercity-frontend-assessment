import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateUser, getUsers } from '../actions/users';
import NavBar from './NavBar';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';
import { useLocation } from 'react-router-dom';
import { FormControl } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  form: {
    marginTop: 120,
    padding: 20,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textfield: {
    margin: 20,
  },
  formControl: {
    width: 500,
    marginBottom: '20px',
  },
}));

const EditUser = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { pathname } = useLocation();
  const id = pathname.replace('/users/edit/', '');

  const user = useSelector((state) => {
    return state.users.users.find((user) => user._id === id);
  });
  const users = useSelector((state) => state.users.users);
  const [userData, setUserData] = useState({
    name: user ? user.name : '',
  });

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  useEffect(() => {}, [users]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUserData({ ...userData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    try {
      dispatch(updateUser(id, userData));
      alert('You have successfully updated this user.');
      // dispatch(clearUser(id));
    } catch (error) {
      console.log(error);
      alert('There was an error submitting the updated user information.');
    }
  };

  if (!user || Object.keys(user).length === 0) {
    return (
      <NavBar>
        <h1>Loading...</h1>
      </NavBar>
    );
  } else {
    return (
      <NavBar>
        <Typography variant='h2' align='center'>
          Update User
        </Typography>

        <form className={classes.form} onSubmit={handleSubmit}>
          <FormControl className={classes.formControl}>
            <TextField
              value={userData.name}
              label='Name'
              name='name'
              onChange={handleChange}
              fullWidth
              className={classes.textfield}
            />
          </FormControl>
          <Button type='submit'>Submit</Button>
        </form>
      </NavBar>
    );
  }
};

export default EditUser;
