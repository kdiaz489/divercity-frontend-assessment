import {
  Card,
  CardContent,
  Container,
  makeStyles,
  Typography,
  Grid,
  Box,
  Button,
} from '@material-ui/core';
import cyan from '@material-ui/core/colors/cyan';
import TeamCard from './TeamCard';
import User from './User';
import NavBar from './NavBar';
import useDepartmentDetails from '../hooks/useDepartmentDetails';
import TeamDetailsDialog from './TeamDetailsDialog';

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: 40,
    paddingBottom: 40,

    height: '100vh',
  },
  title: {
    color: cyan[500],
  },
  card: {
    padding: '12px',
    margin: '40px 0',
  },

  teams: {
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      flexWrap: 'wrap',
    },
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      flexWrap: 'wrap',
    },
  },
}));

const DepartmentDetails = (props) => {
  const { department, open, setOpen, team, setTeam, handleClick, handleClose } =
    useDepartmentDetails(props);
  const classes = useStyles();

  if (Object.keys(department).length === 0 || department === undefined) {
    return (
      <NavBar>
        <Typography variant='h2' align='center'>
          Loading...
        </Typography>
      </NavBar>
    );
  }
  return (
    <NavBar>
      <>
        {team && (
          <TeamDetailsDialog
            team={team}
            open={open}
            setOpen={setOpen}
            handleClose={handleClose}
          />
        )}

        <Container maxWidth='lg' className={classes.root}>
          <Typography variant='h2' align='center'>
            {department.name} Department
          </Typography>
          <Card className={classes.card}>
            <CardContent>
              <Typography variant='h5' component='h2' className={classes.title}>
                In Charge of Department
              </Typography>
              <Grid container spacing={1}>
                <Grid item xs={12} md={6}>
                  <Box
                    display='flex'
                    flexDirection='column'
                    justifyContent='center'>
                    <Typography>Name: {department.inCharge.name}</Typography>

                    <Typography>ID: {department.inCharge._id}</Typography>
                  </Box>
                </Grid>
                <Grid item xs={12} md={6}>
                  <Box
                    display='flex'
                    flexDirection='row'
                    justifyContent='center'>
                    <User />
                  </Box>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
          <Typography variant='h5'>Teams</Typography>
          <Box className={classes.teams}>
            {department.teams.map((team) => (
              <TeamCard
                key={team._id}
                setTeam={setTeam}
                setDialogOpen={setOpen}
                team={team}
              />
            ))}
          </Box>

          <Box
            onClick={handleClick}
            display='flex'
            justifyContent='flex-end'
            alignItems='flex-end'>
            <Button>Edit Department</Button>
          </Box>
        </Container>
      </>
    </NavBar>
  );
};

export default DepartmentDetails;
