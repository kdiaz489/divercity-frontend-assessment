import DepartmentCard from './DepartmentCard';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import NavBar from './NavBar';
import { Button } from '@material-ui/core';
import AddDepartmentDialog from './AddDepartmentDialog';
import useDepartments from '../hooks/useDepartments';

const Departments = () => {
  const { open, setOpen, departments, handleOpen } = useDepartments();

  if (departments.length === 0 || departments === undefined) {
    return (
      <NavBar>
        <Typography variant='h2' align='center'>
          Loading...
        </Typography>
      </NavBar>
    );
  }

  return (
    <NavBar>
      <AddDepartmentDialog open={open} setOpen={setOpen} />
      <Typography variant='h2' align='center'>
        All Departments
      </Typography>
      <Box
        display='flex'
        flexDirection='row'
        flexWrap='wrap'
        justifyContent='space-around'>
        {departments.map((department) => (
          <DepartmentCard key={department._id} department={department} />
        ))}
      </Box>
      <Box display='flex' justifyContent='flex-end' alignItems='flex-end'>
        <Button onClick={handleOpen}>Add Department</Button>
      </Box>
    </NavBar>
  );
};

export default Departments;
