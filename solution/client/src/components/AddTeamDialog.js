import { makeStyles } from '@material-ui/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import Dialog from './Dialog';
import useAddTeamDialog from '../hooks/useAddTeamDialog';

const useStyles = makeStyles((theme) => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textfield: {
    margin: 20,
  },
  formControl: {
    width: '100%',
    marginBottom: '20px',
  },
}));

const AddTeamDialog = (props) => {
  const classes = useStyles();
  const {
    open,
    setOpen,
    users,
    teamData,
    setTeamData,
    handleSubmit,
    handleChange,
    handleClose,
  } = useAddTeamDialog(props);

  return (
    <Dialog
      open={open}
      dialogTitle={'Add Team'}
      dialogText={'Select your team lead and people for your new team.'}
      handleClose={handleClose}
      handleSubmit={handleSubmit}
      setOpen={setOpen}>
      <form className={classes.form} onSubmit={handleSubmit}>
        <FormControl className={classes.formControl}>
          <InputLabel id='teamLead-label'>Team Lead</InputLabel>
          <Select
            className={classes.root}
            labelId='teamLead-label'
            id='teamLead'
            value={teamData.teamLead}
            fullWidth
            onChange={handleChange}
            inputProps={{
              name: 'teamLead',
            }}>
            <MenuItem value=''>
              <em>None</em>
            </MenuItem>
            {users.map((user) => (
              <MenuItem key={user._id} value={user._id}>
                {user.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel id='people-label'>People</InputLabel>
          <Select
            labelId='people-label'
            id='people'
            multiple
            fullWidth
            value={teamData.people}
            onChange={handleChange}
            input={<Input />}
            inputProps={{
              name: 'people',
            }}>
            {users.map((user) => (
              <MenuItem key={user._id} value={user._id}>
                {user.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </form>
    </Dialog>
  );
};

export default AddTeamDialog;
