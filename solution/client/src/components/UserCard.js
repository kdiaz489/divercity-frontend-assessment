import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { useHistory } from 'react-router-dom';
import { Box, IconButton } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import cyan from '@material-ui/core/colors/cyan';

const useStyles = makeStyles({
  root: {
    width: 275,
    margin: '40px 0',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    color: cyan[500],
  },
  pos: {
    marginBottom: 12,
  },
});

const UserCard = ({ user }) => {
  const classes = useStyles();
  const history = useHistory();

  const redirectToEdit = (e) => {
    history.push(`/users/edit/${user._id}`);
  };
  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography
          variant='h5'
          component='h2'
          className={classes.title}
          gutterBottom>
          User
        </Typography>
        <Typography>{user.name}</Typography>

        <Box display='flex' justifyContent='flex-end'>
          <IconButton size='small' onClick={redirectToEdit}>
            <EditIcon />
          </IconButton>
        </Box>
      </CardContent>
    </Card>
  );
};

export default UserCard;
