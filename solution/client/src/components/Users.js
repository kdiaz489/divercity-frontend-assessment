import UserCard from './UserCard';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import NavBar from './NavBar';
import { Button } from '@material-ui/core';
import AddUserDialog from './AddUserDialog';
import useUsers from '../hooks/useUsers';

const Users = (props) => {
  const { open, setOpen, users, handleOpen } = useUsers(props);

  if (users.length === 0 || users === undefined) {
    return (
      <NavBar>
        <Typography variant='h2' align='center'>
          Loading...
        </Typography>
      </NavBar>
    );
  }
  return (
    <NavBar>
      <AddUserDialog open={open} setOpen={setOpen} />
      <Typography variant='h2' style={{ textAlign: 'center' }}>
        All Users
      </Typography>
      <Box
        display='flex'
        flexDirection='row'
        flexWrap='wrap'
        justifyContent='space-around'>
        {users.map((user) => (
          <UserCard key={user._id} user={user} />
        ))}
      </Box>
      <Box
        onClick={handleOpen}
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          alignItems: 'flex-end',
        }}>
        <Button>Add User</Button>
      </Box>
    </NavBar>
  );
};

export default Users;
