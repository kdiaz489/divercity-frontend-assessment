import Dialog from './Dialog';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Box, makeStyles, TextField } from '@material-ui/core';
import Input from '@material-ui/core/Input';
import useAddDepartmentDialog from '../hooks/useAddDepartmentDialog';

const useStyles = makeStyles((theme) => ({
  formControl: {
    width: '100%',
    marginBottom: '20px',
  },
}));

const AddDepartmentDialog = (props) => {
  const classes = useStyles();

  const {
    users,
    teams,
    departmentData,
    setDepartmentData,
    handleChange,
    handleSubmit,
    handleClose,
    open,
    setOpen,
  } = useAddDepartmentDialog(props);

  return (
    <>
      <Dialog
        open={open}
        onClose={handleClose}
        dialogTitle={'Add Department'}
        dialogText={'Add new department'}
        handleSubmit={handleSubmit}
        handleClose={handleClose}
        setOpen={setOpen}
        aria-labelledby='form-dialog-title'>
        <>
          <Box display='flex' flexDirection='column'>
            <FormControl className={classes.formControl}>
              <TextField
                label='Name'
                name='name'
                value={departmentData.name}
                onChange={handleChange}
              />
            </FormControl>
            <FormControl className={classes.formControl}>
              <InputLabel id='inCharge-label'>In Charge</InputLabel>
              <Select
                className={classes.root}
                labelId='inCharge-label'
                id='inCharge'
                value={departmentData.inCharge}
                onChange={handleChange}
                inputProps={{
                  name: 'inCharge',
                }}>
                <MenuItem value=''>
                  <em>None</em>
                </MenuItem>
                {users.map((user) => (
                  <MenuItem key={user._id} value={user._id}>
                    {user.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <FormControl className={classes.formControl}>
              <InputLabel id='teams-label'>Teams</InputLabel>
              <Select
                labelId='teams-label'
                id='teams'
                multiple
                value={departmentData.teams}
                onChange={handleChange}
                input={<Input />}
                inputProps={{
                  name: 'teams',
                }}>
                <MenuItem value=''>
                  <em>None</em>
                </MenuItem>
                {teams.map((team) => (
                  <MenuItem key={team._id} value={team._id}>
                    {team._id}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Box>
        </>
      </Dialog>
    </>
  );
};

export default AddDepartmentDialog;
