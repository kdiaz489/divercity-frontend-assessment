import { makeStyles } from '@material-ui/styles';
import Dialog from './Dialog';
import { TextField } from '@material-ui/core';
import useAddUserDialog from '../hooks/useAddUserDialog';

const useStyles = makeStyles((theme) => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textfield: {
    margin: 20,
  },
}));

const AddUserDialog = (props) => {
  const { open, setOpen } = props;
  const classes = useStyles();
  const { user, handleSubmit, handleChange } = useAddUserDialog(props);

  return (
    <Dialog
      open={open}
      setOpen={setOpen}
      dialogTitle={'Add User'}
      dialogText={'Add user name.'}
      handleSubmit={handleSubmit}>
      <form className={classes.form} onSubmit={handleSubmit}>
        <TextField
          label='Name'
          fullWidth
          name='name'
          value={user.name}
          onChange={handleChange}
        />
      </form>
    </Dialog>
  );
};

export default AddUserDialog;
