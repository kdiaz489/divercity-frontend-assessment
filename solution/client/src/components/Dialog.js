import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Box, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  title: {
    color: theme.palette.primary.main,
    fontSize: 25,
  },
}));

const MyDialog = ({
  open,
  setOpen,
  dialogTitle,
  dialogText,
  children,
  handleSubmit,
  disableSubmit = false,
}) => {
  const classes = useStyles();
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Dialog
        open={open}
        fullWidth={true}
        maxWidth='xs'
        onClose={handleClose}
        aria-labelledby='dialog-title'>
        <DialogTitle
          disableTypography={true}
          classes={{ root: classes.title }}
          id='dialog-title'>
          {dialogTitle}
        </DialogTitle>
        <DialogContent>
          <Box display='flex' flexDirection='column'>
            <DialogContentText>{dialogText}</DialogContentText>
            {children}
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          {disableSubmit ? null : (
            <Button onClick={handleSubmit}>Submit</Button>
          )}
        </DialogActions>
      </Dialog>
    </>
  );
};

export default MyDialog;
