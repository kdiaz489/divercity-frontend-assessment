import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles({
  root: {
    width: 275,
    margin: '40px 0',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

const DepartmentCard = ({ department }) => {
  const classes = useStyles();
  const history = useHistory();
  const handleClick = (e) => {
    e.preventDefault();
    history.push(`/departments/${department._id}`);
  };
  return (
    <Card className={classes.root} onClick={handleClick}>
      <CardContent>
        <Typography
          className={classes.title}
          color='textSecondary'
          gutterBottom>
          Department
        </Typography>
        <Typography variant='h5' component='h2'>
          {department.name}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default DepartmentCard;
