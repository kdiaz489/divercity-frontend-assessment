import NavBar from './NavBar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import useEditTeam from '../hooks/useEditTeam';

const useStyles = makeStyles((theme) => ({
  form: {
    marginTop: 120,
    padding: 20,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textfield: {
    margin: 20,
  },
  formControl: {
    width: 500,
    marginBottom: '20px',
  },
}));

const EditTeam = (props) => {
  const classes = useStyles();
  const { team, teamData, handleSubmit, handleChange, users } =
    useEditTeam(props);

  return (
    <NavBar>
      <Typography variant='h2' align='center'>
        Edit Team
      </Typography>

      <form className={classes.form} onSubmit={handleSubmit}>
        <FormControl className={classes.formControl}>
          <InputLabel id='teamLead-label'>Team Lead</InputLabel>
          <Select
            className={classes.root}
            labelId='teamLead-label'
            id='teamLead'
            value={teamData.teamLead}
            name='teamLead'
            onChange={handleChange}>
            <MenuItem value=''>
              <em>None</em>
            </MenuItem>
            {users.map((user) => (
              <MenuItem key={user._id} value={user._id}>
                {user.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel id='people-label'>People</InputLabel>
          <Select
            labelId='people-label'
            id='people'
            multiple
            name='people'
            value={teamData.people}
            onChange={handleChange}
            input={<Input />}>
            {users.map((user) => (
              <MenuItem key={user._id} value={user._id}>
                {user.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <Button type='submit'>Submit</Button>
      </form>
    </NavBar>
  );
};

export default EditTeam;
