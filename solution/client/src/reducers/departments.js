export default (state = { departments: [], department: {} }, action) => {
  switch (action.type) {
    case 'FETCH_ALL_DEPARTMENTS':
      return { ...state, departments: action.payload };
    case 'FETCH_DEPARTMENT':
      return { ...state, department: action.payload };
    case 'CREATE_DEPARTMENT':
      return { ...state, departments: [...state.departments, action.payload] };

    case 'UPDATE_DEPARTMENT':
      return {
        ...state,
        departments: state.departments.map((department) =>
          department._id === action.payload._id ? action.payload : department
        ),
      };

    default:
      return state;
  }
};
