import { combineReducers } from 'redux';
import departments from './departments';
import users from './users';
import teams from './teams';

export default combineReducers({
  departments,
  users,
  teams,
});
