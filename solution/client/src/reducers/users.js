export default (state = { users: [], user: {} }, action) => {
  switch (action.type) {
    case 'FETCH_ALL_USERS':
      return { ...state, users: action.payload };
    case 'FETCH_USER':
      return { ...state, user: action.payload };
    case 'CLEAR_USER':
      return { ...state, user: {} };
    case 'CREATE_USER':
      return { ...state, users: [...state.users, action.payload] };
    case 'UPDATE_USER':
      return {
        ...state,
        users: state.users.map((user) =>
          user._id === action.payload._id ? action.payload : user
        ),
      };
    default:
      return state;
  }
};
