export default (state = { teams: [], team: {} }, action) => {
  switch (action.type) {
    case 'FETCH_ALL_TEAMS':
      return { ...state, teams: action.payload };
    case 'FETCH_TEAM':
      return { ...state, team: action.payload };
    case 'CREATE_TEAM':
      return { ...state, teams: [...state.teams, action.payload] };
    case 'UPDATE_TEAM':
      return {
        ...state,
        teams: state.teams.map((team) =>
          team._id === action.payload._id ? action.payload : team
        ),
      };

    default:
      return state;
  }
};
