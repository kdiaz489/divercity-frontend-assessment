import axios from 'axios';
const url = 'http://localhost:5000';

export const fetchDepartments = () => axios.get(`${url}/departments`);
export const fetchDepartment = (id) => axios.get(`${url}/departments/${id}`);
export const createDepartment = (newDepartment) =>
  axios.post(`${url}/departments`, newDepartment);
export const updateDepartment = (id, updatedDepartment) =>
  axios.patch(`${url}/departments/${id} `, updatedDepartment);

export const fetchUsers = () => axios.get(`${url}/users`);
export const fetchUser = (id) => axios.get(`${url}/users/${id}`);
export const createUser = (newUser) => axios.post(`${url}/users`, newUser);
export const updateUser = (id, updatedUser) =>
  axios.patch(`${url}/users/${id}`, updatedUser);

export const fetchTeams = () => axios.get(`${url}/teams`);
export const fetchTeam = (id) => axios.get(`${url}/teams/${id}`);
export const createTeam = (newTeam) => axios.post(`${url}/teams/`, newTeam);
export const updateTeam = (id, updatedTeam) =>
  axios.patch(`${url}/teams/${id} `, updatedTeam);
