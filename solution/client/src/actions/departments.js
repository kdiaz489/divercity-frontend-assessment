import * as api from '../api';

// Action Creators
export const getDepartments = () => async (dispatch) => {
  try {
    const { data } = await api.fetchDepartments();
    dispatch({ type: 'FETCH_ALL_DEPARTMENTS', payload: data });
  } catch (error) {
    console.log(error.message);
  }
};

export const getDepartment = (id) => async (dispatch) => {
  try {
    const { data } = await api.fetchDepartment(id);

    dispatch({ type: 'FETCH_DEPARTMENT', payload: data });
  } catch (error) {
    console.log(error.message);
  }
};

export const createDepartment = (department) => async (dispatch) => {
  try {
    const { data } = await api.createDepartment(department);
    dispatch({ type: 'CREATE', payload: data });
  } catch (error) {
    console.log(error.message);
  }
};

export const updateDepartment = (id, department) => async (dispatch) => {
  try {
    let { data } = await api.updateDepartment(id, department);
    dispatch({ type: 'UPDATE', payload: data });
  } catch (error) {
    console.log(error.message);
  }
};
