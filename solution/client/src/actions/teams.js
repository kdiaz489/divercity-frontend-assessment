import * as api from '../api';

// Action Creators
export const getTeams = () => async (dispatch) => {
  try {
    const { data } = await api.fetchTeams();
    dispatch({ type: 'FETCH_ALL_TEAMS', payload: data });
  } catch (error) {
    console.log(error.message);
  }
};

export const getTeam = (id) => async (dispatch) => {
  try {
    const { data } = await api.fetchTeam(id);
    dispatch({ type: 'FETCH_TEAM', payload: data });
  } catch (error) {
    console.log(error.message);
  }
};

export const createTeam = (team) => async (dispatch) => {
  try {
    const { data } = await api.createTeam(team);
    dispatch({ type: 'CREATE_TEAM', payload: data });
  } catch (error) {
    console.log(error.message);
  }
};

export const updateTeam = (id, team) => async (dispatch) => {
  try {
    let { data } = await api.updateTeam(id, team);
    dispatch({ type: 'UPDATE_TEAM', payload: data });
  } catch (error) {
    console.log(error.message);
  }
};
