import * as api from '../api';

export const getUsers = () => async (dispatch) => {
  try {
    const { data } = await api.fetchUsers();

    dispatch({ type: 'FETCH_ALL_USERS', payload: data });
  } catch (error) {
    console.log(error.message);
  }
};

export const getUser = (id) => async (dispatch) => {
  try {
    const { data } = await api.fetchUser(id);

    dispatch({ type: 'FETCH_USER', payload: data });
  } catch (error) {
    console.log(error.message);
  }
};

export const createUser = (team) => async (dispatch) => {
  try {
    const { data } = await api.createUser(team);
    dispatch({ type: 'CREATE_USER', payload: data });
  } catch (error) {
    console.log(error.message);
  }
};

export const updateUser = (id, user) => async (dispatch) => {
  try {
    let { data } = await api.updateUser(id, user);
    dispatch({ type: 'UPDATE_USER', payload: data });
  } catch (error) {
    console.log(error.message);
  }
};

export const clearUser = (id) => async (dispatch) => {
  try {
    dispatch({ type: 'CLEAR_USER', payload: id });
  } catch (error) {
    console.log(error.message);
  }
};
