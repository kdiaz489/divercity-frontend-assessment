import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getUsers } from '../actions/users';
import { createDepartment, getDepartments } from '../actions/departments';
import { getTeams } from '../actions/teams';

const useAddDepartmentDialog = (props) => {
  const { open, setOpen } = props;
  const users = useSelector((state) => state.users.users);
  const teams = useSelector((state) => state.teams.teams);
  const dispatch = useDispatch();
  const [departmentData, setDepartmentData] = useState({
    teams: [],
    inCharge: '',
    name: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setDepartmentData({ ...departmentData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    try {
      dispatch(createDepartment(departmentData));
      alert('You have successfully created a department.');
    } catch (error) {
      alert('There was an error trying to create a department.');
    }
  };

  const handleClose = () => {
    setOpen(false);
  };
  useEffect(() => {
    dispatch(getDepartments());
    dispatch(getTeams());
    dispatch(getUsers());
  }, [dispatch]);

  return {
    users,
    teams,
    departmentData,
    setDepartmentData,
    handleChange,
    handleSubmit,
    handleClose,
    open,
    setOpen,
  };
};

export default useAddDepartmentDialog;
