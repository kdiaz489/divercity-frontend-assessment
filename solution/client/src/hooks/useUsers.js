import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getUsers } from '../actions/users';

const useUsers = () => {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);

  const users = useSelector((state) => {
    return state.users.users;
  });

  const handleOpen = (e) => {
    setOpen(true);
  };

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  return {
    open,
    setOpen,
    users,
    handleOpen,
  };
};

export default useUsers;
