import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getUsers } from '../actions/users';
import { getDepartments, updateDepartment } from '../actions/departments';
import { getTeams } from '../actions/teams';

const useEditDepartment = (props) => {
  const { id } = props.match.params;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getDepartments());
    dispatch(getTeams());
    dispatch(getUsers());
  }, [dispatch, id]);

  const department = useSelector((state) => {
    return state.departments.departments.find(
      (department) => department._id === id
    );
  });

  const users = useSelector((state) => state.users.users);
  const teams = useSelector((state) => state.teams.teams);

  const [departmentData, setDepartmentData] = useState({
    teams: department.teams.map((team) => team._id),
    inCharge: department.inCharge._id,
    name: department.name,
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setDepartmentData({ ...departmentData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    try {
      dispatch(updateDepartment(id, departmentData));
      alert('You have successfully updated this department.');
      // dispatch(clearUser(id));
    } catch (error) {
      console.log(error);
      alert(
        'There was an error submitting the updated department information.'
      );
    }
  };
  return {
    department,
    users,
    teams,
    departmentData,
    setDepartmentData,
    handleChange,
    handleSubmit,
  };
};

export default useEditDepartment;
