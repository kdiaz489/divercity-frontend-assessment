import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getDepartments } from '../actions/departments';
import { getTeams } from '../actions/teams';
import { getUsers } from '../actions/users';

const useDepartments = () => {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const departments = useSelector((state) => {
    return state.departments.departments;
  });

  const handleOpen = () => {
    setOpen(true);
  };

  useEffect(() => {
    dispatch(getDepartments());
  }, [dispatch]);

  return {
    open,
    setOpen,
    departments,
    handleOpen,
  };
};

export default useDepartments;
