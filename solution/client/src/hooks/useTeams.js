import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getTeams } from '../actions/teams';
import { getUsers } from '../actions/users';
import { useHistory } from 'react-router-dom';

const useTeams = (props) => {
  const { id } = props.match.params;
  const history = useHistory();
  const [open, setOpen] = useState(false);
  const [openAddTeam, setOpenAddTeam] = useState(false);

  const [team, setTeam] = useState(null);
  const dispatch = useDispatch();
  const teams = useSelector((state) => state.teams.teams);
  useEffect(() => {
    dispatch(getTeams());
    dispatch(getUsers());
  }, [dispatch, id]);

  const handleOpen = (e) => {
    setOpen(true);
  };

  const handleClick = (e) => {
    history.push(`/teams/edit/${team._id}`);
  };

  const handleAddTeamClick = (e) => {
    setOpenAddTeam(true);
  };

  return {
    open,
    team,
    teams,
    setTeam,
    setOpen,
    handleOpen,
    handleClick,
    openAddTeam,
    setOpenAddTeam,
    handleAddTeamClick,
  };
};
export default useTeams;
