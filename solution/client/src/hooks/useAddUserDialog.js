import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { getUsers, createUser } from '../actions/users';

const useAddUserDialog = (props) => {
  const { open, setOpen } = props;
  const dispatch = useDispatch();

  let initialState = { name: '' };
  const [user, setUser] = useState(initialState);

  const handleSubmit = (e) => {
    e.preventDefault();
    try {
      dispatch(createUser(user));
      alert('You have successfully created a new user.');
    } catch (error) {
      alert('There was an error trying to create a new user.');
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUser({ ...user, [name]: value });
  };

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  return {
    user,
    setUser,

    handleSubmit,
    handleChange,
  };
};

export default useAddUserDialog;
