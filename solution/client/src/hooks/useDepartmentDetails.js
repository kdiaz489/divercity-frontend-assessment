import { useEffect, useState } from 'react';
import { getDepartment, getDepartments } from '../actions/departments';
import { useDispatch, useSelector } from 'react-redux';
import { getTeams } from '../actions/teams';
import { getUsers } from '../actions/users';
import { useHistory } from 'react-router-dom';

const useDepartmentDetails = (props) => {
  const { id } = props.match.params;
  const history = useHistory();
  const [open, setOpen] = useState(false);
  const [team, setTeam] = useState(null);
  const dispatch = useDispatch();
  const department = useSelector((state) => state.departments.department);
  const teams = useSelector((state) => state.teams.teams);
  useEffect(() => {
    dispatch(getDepartment(id));
    dispatch(getDepartments());
    dispatch(getTeams());
    dispatch(getUsers());
  }, [dispatch, id]);

  const handleOpen = (e) => {
    setOpen(true);
  };

  const handleClick = (e) => {
    history.push(`/departments/edit/${department._id}`);
  };

  const handleClose = (e) => {
    setOpen(false);
  };

  return {
    open,
    team,
    teams,
    setTeam,
    setOpen,
    department,
    handleOpen,
    handleClick,
    handleClose,
  };
};

export default useDepartmentDetails;
