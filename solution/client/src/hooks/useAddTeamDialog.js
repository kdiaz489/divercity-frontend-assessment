import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createTeam } from '../actions/teams';
import { useLocation } from 'react-router-dom';

const useAddTeamDialog = (props) => {
  const { open, setOpen } = props;
  const dispatch = useDispatch();
  const { pathname } = useLocation();
  const id = pathname.replace('/teams/edit/', '');

  const users = useSelector((state) => state.users.users);

  const [teamData, setTeamData] = useState({
    people: [],
    teamLead: '',
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    try {
      dispatch(createTeam(teamData));
      alert('You have successfully created a new team.');
    } catch (error) {
      alert('There was an error trying to create a new team.');
    }
  };

  const handleChange = (e) => {
    const { value, name } = e.target;
    setTeamData({ ...teamData, [name]: value });
  };

  const handleClose = (e) => {
    setOpen(false);
  };

  return {
    open,
    setOpen,
    users,
    teamData,
    setTeamData,
    handleSubmit,
    handleChange,
    handleClose,
  };
};

export default useAddTeamDialog;
