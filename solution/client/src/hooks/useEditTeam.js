import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateTeam } from '../actions/teams';
import { useLocation } from 'react-router-dom';

const useEditTeam = (props) => {
  const dispatch = useDispatch();
  const { pathname } = useLocation();
  const id = pathname.replace('/teams/edit/', '');
  const team = useSelector((state) =>
    state.teams.teams.find((team) => team._id === id)
  );
  const users = useSelector((state) => state.users.users);

  const [teamData, setTeamData] = useState({
    people: team.people.map((person) => person._id),
    teamLead: team.teamLead._id,
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    try {
      dispatch(updateTeam(id, teamData));
      alert('You have successfully updated this team.');
    } catch (error) {
      alert('There was an error trying to update this team');
    }
  };

  const handleChange = (e) => {
    const { value, name } = e.target;
    setTeamData({ ...teamData, [name]: value });
  };
  return {
    team,
    teamData,
    handleSubmit,
    handleChange,
    users,
  };
};

export default useEditTeam;
