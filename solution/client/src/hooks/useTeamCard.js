import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { getTeam } from '../actions/teams';

const useTeamCard = ({ team }) => {
  const dispatch = useDispatch();

  const [open, setOpen] = useState(false);
  const [dialogType, setDialogType] = useState('EDIT');

  const handleDetailsClick = (e) => {
    setDialogType('DETAILS');
    dispatch(getTeam(team._id));
    setOpen(true);
  };
  const handleEditClick = (e) => {
    setDialogType('EDIT');
    dispatch(getTeam(team._id));
    setOpen(true);
  };
  return {
    open,
    setOpen,
    dialogType,
    setDialogType,
    handleDetailsClick,
    handleEditClick,
  };
};

export default useTeamCard;
