import './App.css';
import Departments from './components/Departments';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import DepartmentDetails from './components/DepartmentDetails';
import Users from './components/Users';
import Teams from './components/Teams';
import EditUser from './components/EditUser';
import EditDepartment from './components/EditDepartment';
import { theme } from './theme';
import { ThemeProvider } from '@material-ui/core/styles';
import EditTeam from './components/EditTeam';

const App = () => {
  return (
    <Switch>
      <Route
        exact
        path='/departments/:id'
        component={DepartmentDetails}></Route>
      <Route exact path='/' component={Departments}></Route>
      <Route exact path='/users' component={Users}></Route>
      <Route exact path='/teams' component={Teams}></Route>
      <Route exact path='/users/edit/:id' component={EditUser}></Route>
      <Route
        exact
        path='/departments/edit/:id'
        component={EditDepartment}></Route>
      <Route exact path='/teams/edit/:id' component={EditTeam}></Route>
    </Switch>
  );
};

const AppWithProvider = (props) => {
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <App {...props} />
      </ThemeProvider>
    </BrowserRouter>
  );
};

export default AppWithProvider;
