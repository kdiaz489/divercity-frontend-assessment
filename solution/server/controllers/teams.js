import Team from '../models/team.js';
import mongoose from 'mongoose';

export const getTeams = async (req, res) => {
  try {
    const teams = await Team.find().populate('people teamLead');
    res.status(200).json(teams);
  } catch (error) {
    console.log(error.message);
    res.status(400).json({ error: error.message });
  }
};

export const getTeam = async (req, res) => {
  try {
    const { id } = req.params;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).send('No team with that id');
    }
    const team = await Team.findById(id).populate('people teamLead');
    res.status(200).json(team);
  } catch (error) {
    console.log(error.message);
    res.status(400).json({ error: error.message });
  }
};

export const updateTeam = async (req, res) => {
  try {
    const { id } = req.params;
    const team = req.body;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).send('No team with that id');
    }
    const updatedTeam = await Team.findByIdAndUpdate(id, team, {
      new: true,
    }).populate('people teamLead');
    res.status(200).json(updatedTeam);
  } catch (error) {
    console.log(error.message);
    res.status(400).json({ error: error.message });
  }
};

export const createTeam = async (req, res) => {
  try {
    const team = req.body;
    let newTeam = new Team(team);
    await newTeam.save();
    newTeam = newTeam.populate('people teamLead');
    res.status(201).json(newTeam);
  } catch (error) {
    console.log(error.message);
    res.status(400).json({ error: error.message });
  }
};
