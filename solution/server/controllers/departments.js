import Department from '../models/department.js';
import mongoose from 'mongoose';

export const getDepartments = async (req, res) => {
  try {
    const departments = await Department.find().populate('teams inCharge');
    res.status(200).json(departments);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

export const getDepartment = async (req, res) => {
  try {
    const { id } = req.params;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).send('No department with that id');
    }
    const department = await Department.findById(id).populate(
      'teams inCharge people'
    );
    res.status(200).json(department);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

export const updateDepartment = async (req, res) => {
  try {
    const { id } = req.params;
    const department = req.body;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).send('No department with that id');
    }
    const updatedDepartment = await Department.findByIdAndUpdate(
      id,
      department,
      { new: true }
    ).populate('teams inCharge');
    res.status(200).json(updatedDepartment);
  } catch (error) {}
};

export const createDepartment = async (req, res) => {
  try {
    const department = req.body;
    let newDepartment = new Department(department);
    await newDepartment.save();
    newDepartment.populate('teams inCharge');
    res.status(201).json(newDepartment);
  } catch (error) {
    res.status(409).json({ error: error.message });
  }
};
