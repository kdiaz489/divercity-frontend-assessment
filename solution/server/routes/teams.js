import express from 'express';
import {
  getTeams,
  updateTeam,
  createTeam,
  getTeam,
} from '../controllers/teams.js';
const router = express.Router();

router.get('/', getTeams);
router.get('/:id', getTeam);
router.post('/', createTeam);
router.patch('/:id', updateTeam);

export default router;
