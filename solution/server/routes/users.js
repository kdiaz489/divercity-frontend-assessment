import express from 'express';
import {
  getUsers,
  updateUser,
  createUser,
  getUser,
} from '../controllers/users.js';

const router = express.Router();
router.get('/', getUsers);
router.get('/:id', getUser);
router.post('/', createUser);
router.patch('/:id', updateUser);

export default router;
