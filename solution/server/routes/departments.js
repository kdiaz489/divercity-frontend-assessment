import express from 'express';
const router = express.Router();
import {
  getDepartments,
  getDepartment,
  createDepartment,
  updateDepartment,
} from '../controllers/departments.js';

router.get('/', getDepartments);
router.get('/:id', getDepartment);
router.post('/', createDepartment);
router.patch('/:id', updateDepartment);

export default router;
