import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import departments from './routes/departments.js';
import users from './routes/users.js';
import teams from './routes/teams.js';
const app = express();

app.use(express.json({ limit: '30mb', extended: true }));
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use('/departments', departments);
app.use('/users', users);
app.use('/teams', teams);

const CONNECTION_URL =
  'mongodb+srv://kdiaz489:ceK33dx6SCVk856@cluster0.j42bz.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';

const port = process.env.PORT || 5000;

mongoose
  .connect(CONNECTION_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() =>
    app.listen(port, () => console.log(`Server running on PORT: ${port}`))
  )
  .catch((error) => console.log(error.message));

mongoose.set('useFindAndModify', false);
