import mongoose from 'mongoose';

const teamSchema = mongoose.Schema({
  people: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  teamLead: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
});

const Team = mongoose.model('Team', teamSchema);
export default Team;
