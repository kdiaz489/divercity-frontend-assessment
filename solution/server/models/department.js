import mongoose from 'mongoose';

const departmentSchema = mongoose.Schema({
  name: String,
  teams: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Team' }],
  inCharge: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
});

const Department = mongoose.model('Department', departmentSchema);
export default Department;
