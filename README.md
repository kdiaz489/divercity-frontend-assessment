# Getting Started

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Demo

A demo version of this web app was deployed on an AWS EC2 instance.
http://52.14.83.218/

# Prelim Steps

cd into /client and run

### `npm i`

cd into /server and run

### `npm i`

## Available Scripts

In the /solution directory, please run:

### `npm run dev`

Runs the client side of the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Runs the back end of the app under the following url

http://localhost:5000/

## Available API Endpoints

#### Departments

GET
http://localhost:5000/departments

GET
http://localhost:5000/departments/:id

PATCH
http://localhost:5000/departments/:id

POST
http://localhost:5000/departments

#### Teams

GET
http://localhost:5000/teams

GET
http://localhost:5000/teams/:id

PATCH
http://localhost:5000/teams/:id

POST
http://localhost:5000/teams

#### Users

GET
http://localhost:5000/users

GET
http://localhost:5000/users/:id

PATCH
http://localhost:5000/users/:id

POST
http://localhost:5000/users
